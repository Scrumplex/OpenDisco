const gulp = require('gulp');
const pump = require('pump');

// JS
const uglify = require('gulp-uglify');
const babel = require('gulp-babel');


gulp.task('js-prod', function (cb) {
	pump([
			gulp.src('src/*.js'),
			babel({
				presets: ['env'],
				plugins: ['transform-class-properties']
			}),
			uglify(),
			gulp.dest('dist/')
		],
		cb);

});

gulp.task('default', ['js-prod']);