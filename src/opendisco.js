/**
 * Mod object. Contains metadata of modification.
 */
class Mod {
    namespace;
    identifier;
    name;
    version;
    website;
    sources;

    /**
     * This constructor can not be used!
     */
    constructor() {
        throw new Error("Direct initialization not allowed.");
    }

    /**
     * Activates a mod (adds all defined sources to DOM)
     */
    activate() {
        console.debug("[OpenDisco] Activating mod " + this.name + " (" + this.version.text + ")...");
        this.sources.forEach(source => {
            this._activateSource(source);
        })
    }

    /**
     * Activates given source.
     * @param source
     * @private
     */
    _activateSource(source) {
        if (source.type === "css") {
            const elem = document.createElement("link");
            elem.setAttribute("rel", "stylesheet");

            elem.setAttribute("href", source.url);
            if (source.options)
                Object.keys(source.options).forEach(key => {
                    elem.setAttribute(key, source.options["key"]);
                });

            document.head.appendChild(elem);
        } else if (source.type === "js") {
            const elem = document.createElement("script");

            elem.setAttribute("src", source.url);
            if (source.options)
                Object.keys(source.options).forEach(key => {
                    elem.setAttribute(key, source.options["key"]);
                });

            document.body.appendChild(elem);
        }
    }
}

/**
 * Manages mods (Load / Unload, Install, Uninstall)
 */
class ModManager {

    /**
     * @type {Storage}
     * @private
     */
    static _storage = Storages.localStorage;

    static _activatedMods = [];

    /**
     * @type {{loadMod: Array, installMod: Array, activateMod: Array}}
     * @private
     */
    static _eventHandlers = {
        "loadMod": [],
        "installMod": [],
        "activateMod": []
    };

    /**
     * Load and activate all mods present in application _storage
     */
    static loadAllMods() {
        ModManager._storageSetup();
        const remotes = this._storage.get('opendc.remotes');

        if (remotes)
            remotes.forEach((value, id) => {
                ModManager._loadRemote(id);
            });
        return true;
    }

    /**
     * @param type One of "loadMod", "installMod", "activateMod"
     * @param fn function to execute
     */
    static addEventHandler(type, fn) {
        ModManager._eventHandlers[type].push(fn);
    }

    /**
     * Trigger event handlers
     * @param type One of "loadMod", "installMod", "activateMod"
     * @param param Parameter for the event handler
     * @private
     */
    static _triggerEvent(type, param = {}) {
        param.timestamp = Date.now();
        ModManager._eventHandlers[type].forEach((fn) => {
            fn(param)
        });
        console.debug("[OpenDisco] Triggered event " + type + ".");
    }

    /**
     * Installs a mod by providing metadata as base64
     * @param url URL to a mod.meta.json
     * @returns int internal Mod id
     */
    static installMod(url) {
        ModManager._triggerEvent("installMod", {
            "url": url
        });

        const id = ModManager._saveRemote(url);
        ModManager._loadRemote(id);

        return id;
    }

    /**
     * Save a mod to application storage
     * @param url URL to a mod.meta.json
     * @returns int internal Mod id
     * @private
     */
    static _saveRemote(url) {
        ModManager._storageSetup();
        const remotes = ModManager._storage.get('opendc.remotes');

        const id = remotes.push(url) - 1;

        ModManager._storage.set("opendc.remotes", remotes);
        return id;
    }

    static _loadRemote(id) {
        ModManager._storageSetup();
        ModManager._triggerEvent("loadMod", {
            "id": id
        });
        const remotes = ModManager._storage.get('opendc.remotes');

        if (remotes.length <= id) // if index is not in list
            throw new Error("Mod id not found.");

        const url = remotes[id];
        fetch(url)
            .then((response) => {
                return response.json();
            })
            .then(ModManager._activateMod);
    }

    static _activateMod(json) {
        const m = Object.setPrototypeOf(json, Mod.prototype);

        ModManager._triggerEvent("activateMod", {
            "mod": m
        });

        m.activate();
        ModManager._activatedMods.push(m);
    }

    /**
     * Create _storage tree if not present. (Will be called automatically)
     * @private
     */
    static _storageSetup() {
        if (!ModManager._storage.isSet("opendc"))
            ModManager._storage.set("opendc", {
                "remotes": []
            });
    }
}
